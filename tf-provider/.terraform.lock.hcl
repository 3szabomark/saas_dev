# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/oci" {
  version = "4.44.0"
  hashes = [
    "h1:7mcS64DAZF+n2Er4xe2egMD0SCRav6zdAIl4zVdf+YU=",
    "zh:064e1891d1ab9bacb71eddd8cc96b4f7335cd9df40b74e2b280b03cc0647b0a8",
    "zh:30fbf42f6c37d6e1f93cdc5defccc6e47528179b079677fce4cde95f4630dbdb",
    "zh:55b48b6e2095cc20a8924c49a87211f72db5b00779cc36f1c74b69e6de4c030b",
    "zh:5860fe4bf401c948539fb258993f868df8230df235836df3b8a092e638969bc3",
    "zh:60137f96fc90d478c569f698d7fe74892ebfe7dd99e5ef91661afb4188482c07",
    "zh:63e2ad6ad0e1227e38afa705e25b4a7c6aa57c96074900d854f8364c85fb2b4d",
    "zh:6aee5fc6ebf3845ad6c393326155c3baf041a6264d08c7254f19aef3118433da",
    "zh:8056be92a7938ac8df105f03295853db0ffc1e7fdd777838868308f4df61098e",
    "zh:a0871b771a516b2c3090d8a637dd7afa07f42010425809abe2d38a29de0cfd73",
    "zh:e3e460481b518537852515c7e40940cc195745c52baddcdbfa843e011c1b2f5c",
  ]
}
