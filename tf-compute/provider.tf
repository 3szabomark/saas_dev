provider "oci" {
  tenancy_ocid = "ocid1.tenancy.oc1..aaaaaaaayvz5njhkoulidtkjhc2md2bmd7tgb4ljm5ajx2cfgx7choo2pkpa"
  user_ocid = "ocid1.user.oc1..aaaaaaaaivqnmnnz44cwjxqrk6gm7uvnswl6ck5exx2jnf5knccovh6phu6a" 
  private_key_path = "/opt/terraform/oci_free_test/.oci/mark_private.pem"
  fingerprint = "e5:6f:bc:6c:14:ce:3a:bb:92:47:56:be:0c:f9:32:96"
  region = "eu-frankfurt-1"
}
