resource "oci_core_instance" "ubuntu_instance" {
    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[1].name
    compartment_id = "ocid1.compartment.oc1..aaaaaaaarxdjpvlroctfnjfhzxpdrc7pmvxro6gr2gm57jc2qtbhk7uzelxq"
    shape = "VM.Standard.E2.1.Micro"
    source_details {
        source_id = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaagncxdebcdpqug73zdhoo7jko2mqj2nwzzk3ybt6xtkp6oi2p3via"
        source_type = "image"
    }
    display_name = "weblogic_vm_1"
    create_vnic_details {
        assign_public_ip = true
        subnet_id = "ocid1.subnet.oc1.eu-frankfurt-1.aaaaaaaa2bnporwoacxrcg3pabl4zsqaxihmjr77ubsqgiqh6fbpvvn6g2oa"
    }
    metadata = {
        ssh_authorized_keys = file("/root/.ssh/id_rsa.pub")
    } 
    preserve_boot_volume = false
}
